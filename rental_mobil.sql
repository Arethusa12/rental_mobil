-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Jan 2020 pada 06.44
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rental_mobil`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pesan`
--

CREATE TABLE `detail_pesan` (
  `id_detail` int(11) NOT NULL,
  `id_p` int(11) NOT NULL,
  `id_mobil` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nama_pesan` varchar(25) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `hari` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_mobil`
--

CREATE TABLE `input_mobil` (
  `id_p` int(11) NOT NULL,
  `id_mobil` int(11) NOT NULL,
  `plat` varchar(11) NOT NULL,
  `tipe` varchar(15) NOT NULL,
  `tahun` varchar(15) NOT NULL,
  `merk` varchar(15) NOT NULL,
  `nama_m` varchar(30) NOT NULL,
  `bensin` varchar(30) NOT NULL,
  `kursi` varchar(15) NOT NULL,
  `transmisi` varchar(30) NOT NULL,
  `nama_p` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tag` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `input_mobil`
--

INSERT INTO `input_mobil` (`id_p`, `id_mobil`, `plat`, `tipe`, `tahun`, `merk`, `nama_m`, `bensin`, `kursi`, `transmisi`, `nama_p`, `harga`, `gambar`, `tanggal`, `tag`) VALUES
(1, 6, 'F 1216 TF', 'City Car', '2019', 'Nissan', 'Mobilio', 'Pertalite', '4', 'Semi Otomatis', 'RIZKY CODSKY', 900000, '2020-01-28-Mobilio.jpg', '2020-01-28 09:22:26', 'Open');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nik` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `username`, `password`, `nik`) VALUES
(1, 'Arethusa Maulana Kawakibi Hida', 'askthusa@gmail.com', 'thusa', '183e7a3a0130dfa75e60f2a1fb0d47c6', '098993732892'),
(2, 'raihan', 'Raihan@gmail.com', 'raihan', 'daa6b8d04ce72d953d5501adc53ddd82', '12345678');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_pemilik`
--

CREATE TABLE `users_pemilik` (
  `id_p` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `email` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nik` int(14) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_pemilik`
--

INSERT INTO `users_pemilik` (`id_p`, `nama`, `email`, `username`, `password`, `nik`, `gambar`) VALUES
(1, 'Rizky Codsky IntheSky', 'rizzkyardyansya', 'rizky', '49d8712dd6ac9c3745d53cd4be48284c', 2147483647, '2020-01-14.jpg'),
(2, 'thusa', 'thusa@gmail.com', 'thusa', '183e7a3a0130dfa75e60f2a1fb0d47c6', 23456789, '2020-01-15.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_pesan`
--
ALTER TABLE `detail_pesan`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indeks untuk tabel `input_mobil`
--
ALTER TABLE `input_mobil`
  ADD PRIMARY KEY (`id_mobil`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users_pemilik`
--
ALTER TABLE `users_pemilik`
  ADD PRIMARY KEY (`id_p`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_pesan`
--
ALTER TABLE `detail_pesan`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `input_mobil`
--
ALTER TABLE `input_mobil`
  MODIFY `id_mobil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users_pemilik`
--
ALTER TABLE `users_pemilik`
  MODIFY `id_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
