<?php

class m_mobil extends CI_Model{

	var $data;

	function tampil_data(){
		return $this->db->get('input_mobil');
	}

	// function tampil_data(){
	// 	$un = $this->session->userdata('username');
	// 	$idu = $this->db->query("SELECT id_p FROM users_pemilik WHERE username='$un' ");

	// 	$data = $idu->id_p;

	// 	return $this->db->query("SELECT * FROM input_mobil,users_pemilik WHERE input_mobil.id_p = users_pemilik.id_p AND input_mobil.id_p = '$data' ");
	
	// }

	public function tampil_user($id_p)
	{
		$this->db->where('id_p', $id_p);
		return $this->db->get('users_pemilik')->row();
	}

	function id_pemilik(){
		$this->db->query('SELECT * FROM users_pemilik WHERE id_p');
	}		

	function id(){
		$this->db->query('SELECT * FROM input_mobil WHERE id_mobil');
	}

	function input_data($data,$table){
		return $this->db->insert($table,$data);
	}

	function input_pesan($data,$table){
	return $this->db->insert($table,$data);
	}

	function ambil($where,$table){
		return $this->db->get_where($table,$where);
	}
	function update($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function hapus($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function get_user($id){

		$this->db->where('id',$id);
		return $this->db->get('users')->row();
	}

	public function get_mobil($id_mobil)
	{
		$this->db->where('id_mobil', $id_mobil);
		return $this->db->get('input_mobil')->row();
	}

	public function get_nota($id_detail)
	{
		$this->db->where('id_detail', $id_detail);
		return $this->db->get('detail_pesan')->row();
	}

	public function get_pesan($id_pesan)
	{
		$this->db->where('id_detail', $id_pesan);
		return $this->db->get('detail_pesan')->row();
	}

	public function getCatchMobil(){

		$id_mobil = $this->session->userdata("id_mobil");
		return $this->db->query("SELECT * FROM input_mobil WHERE id_mobil = '$id_mobil' ");
	}

	public function join()
	{
		$id = $this->session->userdata('id');
		$this->db->select('*');
		$this->db->from('input_mobil');
		$this->db->join('detail_pesan','detail_pesan.id_mobil=input_mobil.id_mobil');
		$this->db->join('users','detail_pesan.id=users.id');
		$this->db->where('users.id');
		$query = $this->db->query("SELECT * FROM input_mobil JOIN detail_pesan ON input_mobil.id_mobil = detail_pesan.id_mobil JOIN users ON detail_pesan.id = users.id WHERE users.id='$id' ");	
		return $query->result(); 
	}

	public function join_p()
	{
		$id_p = $this->session->userdata('id_p');
		$query = $this->db->query("SELECT input_mobil.id_p,input_mobil.plat,input_mobil.tipe,input_mobil.tahun,input_mobil.merk,input_mobil.nama_m,input_mobil.bensin,input_mobil.kursi,input_mobil.transmisi,input_mobil.nama_p,input_mobil.harga,input_mobil.gambar,input_mobil.id_mobil FROM input_mobil INNER JOIN users_pemilik ON input_mobil.id_p=users_pemilik.id_p WHERE users_pemilik.id_p='$id_p' ");		
		return $query->result(); 
	}

	public function join_d()
	{
		$id_p = $this->session->userdata('id_p');
		$query = $this->db->query("SELECT detail_pesan.id_detail,detail_pesan.nama_pesan,detail_pesan.id_p,detail_pesan.alamat,detail_pesan.email,detail_pesan.hari,input_mobil.plat,input_mobil.nama_m,input_mobil.harga,detail_pesan.total_harga,detail_pesan.bukti,input_mobil.gambar FROM detail_pesan INNER JOIN input_mobil ON detail_pesan.id_mobil=input_mobil.id_mobil INNER JOIN users_pemilik ON detail_pesan.id_p=users_pemilik.id_p WHERE users_pemilik.id_p='$id_p'  ");		
		return $query->result(); 
	}	

	


	function speak(){
		$id_mobil = $this->session->userdata('id_mobil');

		return $this->db->query('SELECT * FROM input_mobil WHERE id_mobil="$id_mobil" ');
	}


}