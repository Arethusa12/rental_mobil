<!DOCTYPE html>
<html>

        
        <?php $this->load->view('Template/header') ?>

    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Carro</span></span><i class="mdi mdi-layers"></i></a>
                    
                </div>

                
            <!-- ========== Header ========== -->

                    <?php $this->load->view('pemilik/header') ?>

            <!-- ========== Header ========== -->

            <!-- ========== Left Sidebar Start ========== -->
            
                    <?php $this->load->view ('pemilik/sidebar') ?>

            <!-- ========== Left Sidebar End   ========== -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"> Input Mobil </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                       
                                        <li class="active">
                                            Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>

					</div>
                        <!-- end row -->
                        

                                <?= $this->session->flashdata('message')?>
                                <form enctype="multipart/form-data" method="post" action="<?php echo base_url('Admin/tambah_mobil')?>">
                                    <input type="hidden" name="tag" value="Open" >
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Tambah Data</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Plat</label>
                                                                <input type="text" class="form-control" name="plat" placeholder="Plat">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Tipe Mobil</label>
                                                                <select class="form-control" name="tipe">
                                                                    <option value="lupa di isi"></option>
                                                                    <option value="Suv">Suv</option>
                                                                    <option value="Mpv">Mpv</option>
                                                                    <option value="City Car">City Car</option>
                                                                    <option value="Sedan">Sedan</option> 
                                                                    <option value="Mini Van">Mini Van</option>
                                                                    <option value="4 WD">4 WD</option>                                                           
                                                                </select>            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Tahun</label>
                                                                 <input type="text" class="form-control" name="tahun" placeholder="Tahun" >
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Merk</label>
                                                                <select class="form-control" name="merk">
                                                                    <option value="Lupa Di isi"></option>
                                                                    <option value="Toyota">Toyota</option>
                                                                    <option value="Honda">Honda</option>
                                                                    <option value="Suzuki">Suzuki</option>
                                                                    <option value="Daihatsu">Daihatsu</option>
                                                                    <option value="Datsun">Datsun</option>
                                                                    <option value="Nissan">Nissan</option>
                                                                    <option value="Mitsubishi">Mitsubishi</option>
                                                                    <option value="KIA">KIA</option>
                                                                    <option value="BMW">BMW</option>
                                                                    <option value="Mercedes Benz">Mercedes Benz</option>
                                                                    <option value="Isuzu">Isuzu</option>
                                                                    <option value="Wuling">Wuling</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <div class="row"> 
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label class=" control-label"> Brand </label>
                                                               <input type="text" class="form-control" name="nama_m">
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class=" control-label">Bensin</label>
                                                                <select class="form-control" name="bensin">
                                                                    <option value="Lupa Di Isi"></option>
                                                                    <option value="Premium">Premium / ok.88</option>
                                                                    <option value="Pertalite">Pertalite / ok.90</option>
                                                                    <option value="Pertamax">Pertamax / ok.92</option>
                                                                    <option value="Pertamax Plus">Pertamax Plus / ok.95</option>
                                                                    <option value="Solar">Solar</option>
                                                                </select>           
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                         <div class="col-md-6">
                                                            <div class="form-group">                                                            
                                                                <label class="control-label"> Pemilik </label>    
                                                                <input type="text" class="form-control" name="nama_p" placeholder="Hendra">
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">  
                                                           <div class="form-group">
                                                            <label class="control-label">Kursi</label>
                                                            <select class="form-control" name="kursi">
                                                                <option value="lupa di isi"></option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>                                                                             
                                                            </select>                     
                                                          </div>
                                                        </div>                     
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Harga/Hari </label>
                                                                <input type="number" class="form-control" name="harga">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class=" control-label"> Transmisi </label>
                                                                <select class="form-control" name="transmisi">
                                                                    <option value="Lupa Di Isi"></option>
                                                                    <option value="Manual">Manual</option>
                                                                    <option value="Matic">Matic</option>
                                                                    <option value="Semi Otomatis">Semi Otomatis</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Gambar </label>
                                                                <input type="file" class="form-control" name="gambar">
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>

                                                <div class="modal-footer">
                                                   <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div><!-- /.modal Tambah -->

                                </form>

                            <?php foreach ($input_mobil as $row) { ?>
                                <div id="modalupdate<?= $row->id_mobil ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Modal Content is Responsive</h4>
                                                </div>
                                                <form method="post" action="<?php echo base_url('Admin/proses_edit') ?>" >
                                                    <input type="hidden" name="id_mobil" value="<?= $row->id_mobil ?>">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Plat</label>
                                                                <input type="text" class="form-control" value="<?= $row->plat ?>" name="plat" placeholder="Plat">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Tipe Mobil</label>
                                                                <select class="form-control" name="tipe" >
                                                                    <option value="<?= $row->tipe ?>"><?= $row->tipe ?></option>
                                                                    <option value="Suv">Suv</option>
                                                                    <option value="Mpv">Mpv</option>
                                                                    <option value="City Car">City Car</option>
                                                                    <option value="Double Cabin">Double Cabin</option>
                                                                    <option value="Sedan">Sedan</option> 
                                                                    <option value="Mini Van">Mini Van</option>
                                                                    <option value="4 WD">4 WD</option>                                                           
                                                                </select>            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Tahun</label>
                                                                 <input type="text" class="form-control" name="tahun" placeholder="Tahun" value="<?= $row->tahun ?>" >
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Merk</label>
                                                                <select class="form-control" name="merk">
                                                                    <option value="<?= $row->tipe ?>"><?= $row->merk ?></option>
                                                                    <option value="Toyota">Toyota</option>
                                                                    <option value="Honda">Honda</option>
                                                                    <option value="Suzuki">Suzuki</option>
                                                                    <option value="Daihatsu">Daihatsu</option>
                                                                    <option value="Datsun">Datsun</option>
                                                                    <option value="Nissan">Nissan</option>
                                                                    <option value="Mitsubishi">Mitsubishi</option>
                                                                    <option value="KIA">KIA</option>
                                                                    <option value="BMW">BMW</option>
                                                                    <option value="Mercedes Benz">Mercedes Benz</option>
                                                                    <option value="Isuzu">Isuzu</option>
                                                                    <option value="Wuling">Wuling</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <div class="row"> 
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                               <label class=" control-label"> Brand </label>
                                                               <input type="text" class="form-control" name="nama_m" value="<?= $row->nama_m ?>">
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class=" control-label">Bensin</label>
                                                                <select class="form-control" name="bensin">
                                                                    <option value="<?= $row->bensin?>"><?= $row->bensin?></option>
                                                                    <option value="Premium">Premium / ok.88</option>
                                                                    <option value="Pertalite">Pertalite / ok.90</option>
                                                                    <option value="Pertamax">Pertamax / ok.92</option>
                                                                    <option value="Pertamax Plus">Pertamax Plus / ok.95</option>
                                                                    <option value="Solar">Solar</option>
                                                                </select>           
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                         <div class="col-md-6">
                                                            <div class="form-group">                                                            
                                                                <label class="control-label"> Pemilik </label>    
                                                                <input type="text" class="form-control" name="nama_p" placeholder="Hendra" value="<?= $row->nama_p ?>">
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">  
                                                           <div class="form-group">
                                                            <label class="control-label">Kursi</label>
                                                            <select class="form-control" name="kursi">
                                                                <option value="<?= $row->kursi?>"><?= $row->kursi?></option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>                                                                             
                                                            </select>                     
                                                          </div>
                                                        </div>                     
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Harga/Hari </label>
                                                                <input type="number" class="form-control" name="harga" value="<?= $row->harga?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class=" control-label"> Transmisi </label>
                                                                <select class="form-control" name="transmisi">
                                                                    <option value="<?= $row->transmisi?>"><?= $row->transmisi?></option>
                                                                    <option value="Manual">Manual</option>
                                                                    <option value="Matic">Matic</option>
                                                                    <option value="Semi Otomatis">Semi Otomatis</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>

                                                <div class="modal-footer">
                                                   <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal Update -->
                                </form>
                            <?php } ?>
                            

                                  
                                   
                            
                            <div class="col-md-12">
                                            <div class="demo-box m-t-20">

                                            <?php 
                                                $data = $this->session->flashdata('sukses');

                                                if ($data != '') { ?>
                                                    <div id="notifikasi" class="alert alert-success"><strong>Sukses! </strong>
                                                        <?php echo $data ?>
                                                    </div>
                                              <?php  
                                                }
                                             ?>

                                               <?php 
                                                $data = $this->session->flashdata('error');

                                                if ($data != '') { ?>
                                                    <div id="notifikasi" class="alert alert-danger"><strong>Error! </strong>
                                                        <?php echo $data ?>
                                                    </div>
                                              <?php  
                                                }
                                             ?>

                                                

                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="m-b-30">
                                            <button  data-toggle="modal" data-target="#myModal" class="btn btn-success waves-effect waves-light">Add <i class="mdi mdi-plus-circle-outline"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <table class="table table-striped add-edit-table table-bordered" id="datatable-editable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                                <th>Plat Mobil</th>
                                                                <th>Tipe</th>
                                                                <th>Tahun</th>
                                                                <th>Merk Mobil</th>
                                                                <th>Brand</th>
                                                                <th>Bensin</th>
                                                                <th>Kursi</th>
                                                                <th>Transmisi</th>
                                                                <th>Nama Pemilik</th>
                                                                <th>Harga</th>
                                                                <th>Gambar</th>
                                                               
                                                                <th>Aksi</th>
                                            </tr>

                                                                <?php 
                                                             $id = 1;
                                                             foreach ($input_mobil as $u) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $id++?></td>
                                                                <td><?php echo $u->plat?></td>
                                                                <td><?php echo $u->tipe?></td>
                                                                <td><?php echo $u->tahun?></td>
                                                                <td><?php echo $u->merk?></td>
                                                                <td><?php echo $u->nama_m?></td>
                                                                <td><?php echo $u->bensin?></td>
                                                                <td><?php echo $u->kursi?></td>
                                                                <td><?php echo $u->transmisi?></td>
                                                                <td><?php echo $u->nama_p?></td>
                                                                <td><?php echo $u->harga?></td>
                                                                <td><img src="<?= base_url('gambar/'. $u->gambar) ?>" style="width: 100px; height: 50;"></td>
                                                                
                                                                <td>
                                                                <a onclick="del(<?=$u->id_mobil?>)" class="btn btn-default waves-effect waves-light btn-sm tombol-hapus">Hapus</a>

                                                                <a class="btn btn-default waves-effect waves-light btn-sm" data-target="#modalupdate<?= $u->id_mobil ?>" data-toggle="modal" > Edit </a> 
                                                                </td>

                                                                                
                            
                                                            </tr>      
                                                           <?php  }  ?>  
                                        </thead>
                                        <tbody>
                                
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->

                        
                             
                    </div> <!-- container -->

                </div> <!-- content -->


                <footer class="footer text-right">
                    © El Carro.
                </footer>

            </div>           



            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php $this->load->view('Template/footer') ?>

    </body>
<script>//Swal Delete Expertise
     function del(id) {
         swal({
  title: "Are You Sure Want to Delete This Data?",
  text: "This data will affect other data, and Your will not be able to recover this Data! ",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},
function(){
    location.href="<?php echo base_url().'Admin/hapus/';?>"+id;

});
}
</script>
</html>