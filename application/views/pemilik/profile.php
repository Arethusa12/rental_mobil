<!DOCTYPE html>
<html>

        
        <?php $this->load->view('Template/header') ?>

    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>Zir<span>cos</span></span><i class="mdi mdi-layers"></i></a>
                    
                </div>

                
            <!-- ========== Header ========== -->

                    <?php $this->load->view('pemilik/header') ?>

            <!-- ========== Header ========== -->

            <!-- ========== Left Sidebar Start ========== -->
            
                    <?php $this->load->view ('pemilik/sidebar') ?>

            <!-- ========== Left Sidebar End   ========== -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"> My Profile </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                       
                                        <li class="active">
                                            Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
                    <?php 
foreach ($accounts as $acc) {}
    

 ?>
                        <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumb-xl member-thumb m-b-10 center-block text-center">
                                                       <a class="car-hover" href="<?= base_url('gambar/'. $acc->gambar) ?>">
                                                        <img src="<?= base_url('gambar/'. $acc->gambar) ?>" style="width: 100px;" alt="JSOFT">                                     
                                                    </a> 
                                                    </div>

                                                    <div class="">
                                                        <h4 class="m-b-5"><?php echo $acc->nama?></h4>
                                                        
                                                    </div>

                                                    <hr/>

                                                    <div class="text-left">
                                                        <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $acc->email ?></span></p>

                                                    </div>

                                                    <ul class="social-links list-inline m-t-30">
                                                        <li>
                                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                                        </li>
                                                        <li>
                                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                                        </li>
                                                        <li>
                                                            <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                                        </li>
                                                    </ul>

                                                </div>

                                            </div> <!-- end card-box -->
    
                              

                      
                        
                             
                    </div> <!-- container -->

                </div> <!-- content -->


                <footer class="footer text-right">
                    © El Carro.
                </footer>

            </div>           



            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            
        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php $this->load->view('Template/footer') ?>

    </body>
</html>