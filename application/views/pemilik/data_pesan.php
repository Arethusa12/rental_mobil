<!DOCTYPE html>
<html>

        
        <?php $this->load->view('Template/header') ?>

    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                                    <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>El<span>Carro</span></span><i class="mdi mdi-layers"></i></a>
                    
                </div>

            <!-- ========== Header ========== -->

                    <?php $this->load->view('pemilik/header') ?>

            <!-- ========== Header ========== -->

            <!-- ========== Left Sidebar Start ========== -->
            
                    <?php $this->load->view ('pemilik/sidebar') ?>

            <!-- ========== Left Sidebar End   ========== -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"> Data Pemesan </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="">Home</a>
                                        </li>
                                       
                                        <li class="active">
                                            Page
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                            <?php foreach ($trans as $row) { ?>
                                <div id="modalupdate<?= $row->id_detail ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Modal Content is Responsive</h4>
                                                </div>
                                                <form method="post" action="<?php echo base_url('Admin/proses_edit_t') ?>" >
                                                    <input type="hidden" name="id_detail" value="<?= $row->id_detail ?>">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Pemesan </label>
                                                                <input type="text" class="form-control" name="nama" value="<?= $row->nama?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Hari </label>
                                                                <input type="number" class="form-control" name="hari" value="<?= $row->hari?>">
                                                            </div>
                                                        </div>                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Alamat </label>
                                                                <input type="text" class="form-control" name="alamat" value="<?= $row->alamat?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label"> Email </label>
                                                                <input type="email" class="form-control" name="email" value="<?= $row->email?>">
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>

                                                <div class="modal-footer">
                                                   <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal Update -->
                                </form>
                            <?php } ?>
                            <div class="col-md-12">
                                            <div class="demo-box m-t-20">

                                            <?php 
                                                $data = $this->session->flashdata('sukses');

                                                if ($data != '') { ?>
                                                    <div id="notifikasi" class="alert alert-success"><strong>Sukses! </strong>
                                                        <?php echo $data ?>
                                                    </div>
                                              <?php  
                                                }
                                             ?>

                                               <?php 
                                                $data = $this->session->flashdata('error');

                                                if ($data != '') { ?>
                                                    <div id="notifikasi" class="alert alert-danger"><strong>Error! </strong>
                                                        <?php echo $data ?>
                                                    </div>
                                              <?php  
                                                }
                                             ?>

                                                

                                        <div class="panel">

                            <div class="panel-body">
                                <div class="row">
                                </div>

                                <div class="">
                                    <table class="table table-striped add-edit-table table-bordered" id="datatable-editable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                                <th>Pemesan</th>
                                                                <th>Alamat</th>
                                                                <th>Email</th>
                                                                <th>Hari</th>
                                                                <th>Plat Mobil</th>
                                                                <th>Brand</th>
                                                                <th>Harga</th>
                                                                <th>Total Harga</th>
                                                                <th>Gambar</th>
                                                                <th>Bukti Pembayaran</th>
                                            </tr>

                                                                <?php 
                                                             $id = 1;
                                                             foreach ($trans as $u) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $id++?></td>
                                                                <td><?php echo $u->nama_pesan?></td>
                                                                <td><?php echo $u->alamat?></td>
                                                                <td><?php echo $u->email?></td>
                                                                <td><?php echo $u->hari?></td>
                                                                <td><?php echo $u->plat?></td>
                                                                <td><?php echo $u->nama_m?></td>
                                                                <td><?php echo $u->harga?></td>
                                                                <td><?php echo $u->total_harga?></td>
                                                                <td><img src="<?= base_url('gambar/'. $u->gambar) ?>" style="width: 100px; height: 50;"></td>
                                                                <td><img src="<?= base_url('gambar/'. $u->bukti) ?>" style="width: 100px; height: 50;"></td>
                                                                
                                                                <td>
                                                                

                                                                                
                            
                                                            </tr>      
                                                           <?php  }  ?>  
                                        </thead>
                                        <tbody>
                                
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->

                        
                             
                    </div> <!-- container -->

                </div> <!-- content -->


                <footer class="footer text-right">
                    © El Carro.
                </footer>

            </div>           



            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <?php $this->load->view('Template/footer') ?>

    </body>
</html>