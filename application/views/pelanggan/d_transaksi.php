<?php $this->load->view('pelanggan/header')?>

<body class="loader-active">

    <!--== Page Title Area Start ==-->
    <section id="page-title-area" class="section-padding overlay">
        <div class="container">
            <div class="row">
                <!-- Page Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>Order Detail</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                    </div>
                </div>
                <!-- Page Title End -->
            </div>
        </div>
    </section>
    <!--== Page Title Area End ==-->
    <?php foreach ($trans as $t) { ?>
    <!--== Car List Area Start ==-->
    <section id="car-list-area" class="section-padding">
        <div class="container">
            <div class="row">           
                <!-- Car List Content Start -->
                <div class="col-lg-8">
                    <div class="car-list-content">
                        <!-- Single Car Start -->
                        <div class="single-car-wrap">
                            <div class="row">
                                <!-- Single Car Thumbnail -->
                                <div class="col-lg-5">
                                    <div class="single-car-preview">
                                        <img src="<?= base_url('gambar/'.$t->gambar) ?>" >
                                    </div>
                                </div>
                                <!-- Single Car Thumbnail -->

                                <!-- Single Car Info -->
                                <div class="col-lg-7">
                                    <div class="display-table">
                                        <div class="display-table-cell">
                                            <div class="car-list-info">
                                                <h2><a href="#"><?php echo $t->nama_m ?></a></h2>
                                                <h5>Price   :<?php echo $t->total_harga?> Rp Rent /per a day</h5>
                                                <h5>Owner : <?php echo $t->nama_p?></h5>
                                                <a href="<?= base_url('pelanggan/struk/'.$t->id_detail)?>" class="rent-btn">Print Invoice</a>
                                                <?php if($t->tag == "Open") { ?>
                                                
                                            <?php }else{ ?>

                                                <a href="<?= base_url('pelanggan/beres')?>" class="rent-btn">Order Done</a>

                                           <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Car info -->
                            </div>
                        </div>
                        <!-- Single Car End -->
                    </div>
                </div>       
                <!-- Car List Content End -->

                <!-- Sidebar Area Start -->
                <div class="col-lg-4">
                    <div class="sidebar-content-wrap m-t-50">
                        <!-- Single Sidebar Start -->
                        <div class="single-sidebar">
                            <h3>Informations</h3>

                            <div class="sidebar-body">
                                <p><i class="fa fa-odnoklassniki"></i>Order Name      : <?php echo $t->nama_pesan?></p>
                                <p><i class="fa fa-map"></i>Addres : <?php echo $t->alamat?></p>
                                <p><i class="fa fa-envelope"></i> Email   : <?php echo $t->email?> </p>
                                <p><i class="fa fa-calendar-check-o"></i> Order Day   : <?php echo $t->hari?></p>
                            </div>
                        </div>
                        <!-- Single Sidebar End -->
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>
        </div>
    </section>
    <?php } ?>
    <!--== Car List Area End ==-->

    <?php $this->load->view('pelanggan/footer')?>

</body>

</html>