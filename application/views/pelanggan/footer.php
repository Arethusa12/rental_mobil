<!--== Footer Area Start ==-->
    <section id="footer-area">
        <!-- Footer Widget Start -->
        <div class="footer-widget-area">
            <div class="container">
                <div class="row">
                    
                    <!-- Single Footer Widget Start -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-footer-widget">
                            <h2>get touch</h2>
                            <div class="widget-body">
                                

                                <ul class="get-touch">
                                    <li><i class="fa fa-map-marker"></i> Cibereum Hulur Sukabumi, Indonesia</li>
                                    <li><i class="fa fa-mobile"></i> (+62) 812873974629</li>
                                    <li><i class="fa fa-envelope"></i> askthusa@gmail.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Single Footer Widget End -->
                </div>
            </div>
        </div>
        <!-- Footer Widget End -->

        <!-- Footer Bottom Start -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Bottom End -->
    </section>
    <!--== Footer Area End ==-->

        <!--== Scroll Top Area Start ==-->
    <div class="scroll-top">
        <img src="<?php echo base_url('assets/img/scroll-top.png')?>" alt="JSOFT">
    </div>
    <!--== Scroll Top Area End ==-->

        <!--=======================Javascript============================-->
    <!--=== Jquery Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
    <!--=== Jquery Migrate Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-migrate.min.js')?>"></script>
    <!--=== Popper Min Js ===-->
    <script src="<?php echo base_url('assets/js/popper.min.js')?>"></script>
    <!--=== Bootstrap Min Js ===-->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <!--=== Gijgo Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/gijgo.js')?>"></script>
    <!--=== Vegas Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/vegas.min.js')?>"></script>
    <!--=== Isotope Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/isotope.min.js')?>"></script>
    <!--=== Owl Caousel Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/owl.carousel.min.js')?>"></script>
    <!--=== Waypoint Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/waypoints.min.js')?>"></script>
    <!--=== CounTotop Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/counterup.min.js')?>"></script>
    <!--=== YtPlayer Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/mb.YTPlayer.js')?>"></script>
    <!--=== Magnific Popup Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/magnific-popup.min.js')?>"></script>
    <!--=== Slicknav Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/slicknav.min.js')?>"></script>

    <!--=== Mian Js ===-->
    <script src="<?php echo base_url('assets/js/main.js')?>"></script>
