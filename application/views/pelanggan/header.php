<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <title>ElCarro</title>

    <!--=== Bootstrap CSS ===-->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--=== Slicknav CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/slicknav.min.css')?>" rel="stylesheet">
    <!--=== Magnific Popup CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/magnific-popup.css')?>" rel="stylesheet">
    <!--=== Owl Carousel CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/owl.carousel.min.css')?>" rel="stylesheet">
    <!--=== Gijgo CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/gijgo.css')?>" rel="stylesheet">
    <!--=== FontAwesome CSS ===-->
    <link href="<?php echo base_url('assets/css/font-awesome.css')?>" rel="stylesheet">
    <!--=== Theme Reset CSS ===-->
    <link href="<?php echo base_url('assets/css/reset.css')?>" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="<?php echo base_url('assets/style.css')?>" rel="stylesheet">
    <!--=== Responsive CSS ===-->
    <link href="<?php echo base_url('assets/css/responsive.css')?>" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<!--== Header Area Start ==-->
    <header id="header-area" class="fixed-top">
        <!--== Header Top Start ==-->
        <div id="header-top" class="d-none d-xl-block">
            <div class="container">
                <div class="row">
                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-left">
                        <i class="fa fa-map-marker"></i> Cibereum Sukabumi, Indonesia
                    </div>
                    <!--== Single HeaderTop End ==-->

                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-mobile"></i> (+62) 812873974629
                    </div>
                    <!--== Single HeaderTop End ==-->

                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-clock-o"></i> Mon-Sun 09.00 - 17.00
                    </div>
                    <!--== Single HeaderTop End ==-->

                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-center">
                        <?php 
                            if ($this->session->userdata('status') == 'login') { ?>
                                <a href="<?php echo base_url('Welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout </a>
                        <?php
                            }
                         ?>
                        
                    </div>
                    <!--== Single HeaderTop End ==-->
                </div>
            </div>
        </div>
        <!--== Header Top End ==-->

            <!--== Preloader Area Start ==-->
        <div class="preloader">
            <div class="preloader-spinner">
                <div class="loader-content">
                    <img src="<?php echo base_url('assets/img/preloader.gif')?>" alt="JSOFT">
                </div>
            </div>
        </div>
    <!--== Preloader Area End ==-->

        <!--== Header Bottom Start ==-->
        <div id="header-bottom">
            <div class="container">
                <div class="row">
                    <!--== Logo Start ==-->
                    <div class="col-lg-4">
                        
                    </div>
                    <!--== Logo End ==-->

                    <!--== Main Menu Start ==-->
                    <div class="col-lg-8 d-none d-xl-block">
                        <nav class="mainmenu alignright">
                            <ul>
                                <li><a href="<?php echo base_url('Welcome/index')?>">Home</a>
                                    
                                </li>
                                
                               
                                
                            </ul>
                        </nav>
                    </div>
                    <!--== Main Menu End ==-->
                </div>
            </div>
        </div>
        <!--== Header Bottom End ==-->
    </header>
    <!--== Header Area End ==-->