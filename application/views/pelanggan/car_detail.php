<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <title>ElCarro</title>

    <!--=== Bootstrap CSS ===-->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--=== Slicknav CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/slicknav.min.css')?>" rel="stylesheet">
    <!--=== Magnific Popup CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/magnific-popup.css')?>" rel="stylesheet">
    <!--=== Owl Carousel CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/owl.carousel.min.css')?>" rel="stylesheet">
    <!--=== Gijgo CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/gijgo.css')?>" rel="stylesheet">
    <!--=== FontAwesome CSS ===-->
    <link href="<?php echo base_url('assets/css/font-awesome.css')?>" rel="stylesheet">
    <!--=== Theme Reset CSS ===-->
    <link href="<?php echo base_url('assets/css/reset.css')?>" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="<?php echo base_url('assets/style.css')?>" rel="stylesheet">
    <!--=== Responsive CSS ===-->
    <link href="<?php echo base_url('assets/css/responsive.css')?>" rel="stylesheet">


    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="loader-active">

    <!--== Preloader Area Start ==-->
    <div class="preloader">
        <div class="preloader-spinner">
            <div class="loader-content">
                <img src="<?php echo base_url('assets/img/preloader.gif')?>" alt="JSOFT">
            </div>
        </div>
    </div>
    <!--== Preloader Area End ==-->

    <!--== Header Area Start ==-->
    
             <?php $this->load->view('pelanggan/header') ?>

    <!--== Header Area End ==-->

    <!--== Page Title Area Start ==-->
    <section id="page-title-area" class="section-padding overlay">
        <div class="container">
            <div class="row">
                <!-- Page Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>Cars Detail</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                        <p></p>
                    </div>
                </div>
                <!-- Page Title End -->
            </div>
        </div>
    </section>
    <!--== Page Title Area End ==-->
 
    <!--== Car List Area Start ==-->
    <section id="car-list-area" class="section-padding">
        <div class="container">
            <div class="row">
                
                <!-- Car List Content Start -->
                <div class="col-lg-8">
                    <div class="car-details-content">
                        <h2><?php echo $mobil->nama_m ?> <span class="price">Price <b> Rp <?php echo $mobil->harga?> / Hari </b></span></h2>
                        <div class="car-preview-crousel">
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                        </div>
                        <div class="car-details-info">
                           
                            <div class="technical-info">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="tech-info-table">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Car Id</th>
                                                    <th><?php echo $mobil->plat ?></th>
                                                    
                                                </tr>
                                                <tr>
                                                    <th>Type Car</th>
                                                    <th><?php echo $mobil->tipe ?></th>
                                                   
                                                </tr>
                                                <tr>
                                                    <th>Car Year</th>
                                                    <th><?php echo $mobil->tahun ?></th>
                                                    
                                                </tr>
                                                <tr>
                                                    <th>Owner Name</th>
                                                    <th><?php echo $mobil->nama_p ?></th>
                                            
                                                </tr>
                                                <tr>
                                                    <th>Price/Day</th>
                                                    <th><?php echo $mobil->harga ?></th>
                                                </tr>
                                                <tr>
                                                    <th>Brand</th>
                                                    <th><?php echo $mobil->merk ?></th>
                                                </tr>
                                                <tr>
                                                    <th>Fuel Type</th>
                                                    <th><?php echo $mobil->bensin ?></th>
                                                    
                                                </tr>
                                                <tr>
                                                    <th>Seat</th>
                                                    <th><?php echo $mobil->kursi ?></th>
                                                    
                                                </tr>
                                                <tr>
                                                    <th>Transmission</th>
                                                    <th><?php echo $mobil->transmisi ?></th>
                                                    
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>            
                        </div>
                    </div>
                </div>
                <!-- Car List Content End -->

                <!-- Sidebar Area Start -->
                <div class="col-lg-4">
                    <div class="sidebar-content-wrap m-t-50">
                        <!-- Single Sidebar Start -->
                        <div class="single-sidebar">
                            <h3>For More Informations</h3>

                            <div class="sidebar-body">
                                <p><i class="fa fa-mobile"></i> (+62) 812873974629 </p>
                                <p><i class="fa fa-clock-o"></i> Mon-Sun 09.00 - 17.00</p>
                            </div>
                        </div>                       
                        <div class="single-sidebar">
                            <h3>Book This Car Now</h3>

                            <div class="sidebar-body">
                                <p><i class="fa fa-mobile"></i> (+62) 812873974629 </p>
                                <?php 
                                    if ($this->session->userdata('status') == 'login') { ?>
                                        <a href="<?php echo base_url('Pelanggan/catchMobil/'.$mobil->id_mobil)?>" type="submit" class="rent-btn">Book Now</a>     
                                <?php    
                                    } else { ?>
                                        <a href="<?php echo base_url('Login/sepik/'.$mobil->id_mobil)?>" class="rent-btn">Book Now</a>
                                <?php
                                    }
                                    
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>
        </div>
    </section>


    <!--== Car List Area End ==-->

    <!--== Footer Area Start ==-->
    
            <?php $this->load->view('pelanggan/footer') ?>

    <!--== Footer Area End ==-->

    <!--== Scroll Top Area Start ==-->
    <div class="scroll-top">
        <img src="<?php echo base_url('assets/img/scroll-top.png')?>" alt="JSOFT">
    </div>
    <!--== Scroll Top Area End ==-->

    <!--=======================Javascript============================-->
    <!--=== Jquery Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
    <!--=== Jquery Migrate Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-migrate.min.js')?>"></script>
    <!--=== Popper Min Js ===-->
    <script src="<?php echo base_url('assets/js/popper.min.js')?>"></script>
    <!--=== Bootstrap Min Js ===-->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <!--=== Gijgo Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/gijgo.js')?>"></script>
    <!--=== Vegas Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/vegas.min.js')?>"></script>
    <!--=== Isotope Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/isotope.min.js')?>"></script>
    <!--=== Owl Caousel Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/owl.carousel.min.js')?>"></script>
    <!--=== Waypoint Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/waypoints.min.js')?>"></script>
    <!--=== CounTotop Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/counterup.min.js')?>"></script>
    <!--=== YtPlayer Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/mb.YTPlayer.js')?>"></script>
    <!--=== Magnific Popup Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/magnific-popup.min.js')?>"></script>
    <!--=== Slicknav Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/slicknav.min.js')?>"></script>

    <!--=== Mian Js ===-->
    <script src="<?php echo base_url('assets/js/main.js')?>"></script>

</body>

</html>