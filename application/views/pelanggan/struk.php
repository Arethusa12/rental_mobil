<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url('admin/assets/images/favicon.ico')?>">
        <!-- App title -->
       

        <!-- App css -->
        <link href="<?=base_url('admin/assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/core.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/components.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/icons.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/menu.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('admin/assets/css/responsive.css')?>" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?= base_url('admin//plugins/switchery/switchery.min.css')?>">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url('admin/assets/js/modernizr.min.js')?>"></script>

    </head>


    <body class="widescreen fixed-left-void">

        <!-- Begin page -->
        <div id="wrapper" class="enlarged forced">

            <!-- Top Bar Start -->
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- end row -->


                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h3><img src="assets/images/logo_sm.png" alt="" height="24"> Receipt </h3>
                                            </div>
                                            <div class="pull-right">
                                                <h4> Date  <br>
                                                    <p>

                                                    <?php echo  date("Y-m-d h:i:sa") . "<br>"; ?>
                                                        
                                                    </p>
                                                </h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong>El Carro. Corp</strong><br>
                                                      Sukabumi, Jawa Barat<br>
                                                      Baros, 07789<br>
                                                      <abbr title="Phone">Phone:</abbr> (62)081317195090
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Order Date: </strong><?php echo $nota->tanggal?></p>
                                                    <p><strong>Order Status: </strong> <span class="label label-success">Succes</span></p>
                                                    <p><strong>Order ID: </strong> <?php echo $nota->id_detail ?> </p>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->

                                        <div class="m-h-50"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table m-t-30">
                                                        <thead>
                                                            <th>Pemesan</th>
                                                                <th>Alamat</th>
                                                                <th>Email</th>
                                                                
                                                                
                                                        </tr></thead>
                                                        <tbody>
                                                            
                                                            <tr>
                                                                
                                                                <td><?php echo $nota->nama_pesan?></td>
                                                                <td><?php echo $nota->alamat?></td>
                                                                <td><?php echo $nota->email?></td>
                                                                
                                                                
                                                                

                                                                                
                            
                                                            </tr>      
                                                           
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="clearfix m-t-40">
                                                    <h5 class="small text-inverse font-600"></h5>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                                <p class="text-right"><b>Days : </b> <?php echo $nota->hari ?>  </p>
                                                <p class="text-right">Price/Day: <?php echo $nota->harga?></p>
                                                <hr>
                                                <h3 class="text-right">RP <?php echo $nota->total_harga?></h3>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

               

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?= base_url('admin/assets/js/jquery.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/detect.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/fastclick.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/waves.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?= base_url('admin/plugins/switchery/switchery.min.js')?>"></script>

        <!-- App js -->
        <script src="<?= base_url('admin/assets/js/jquery.core.js')?>"></script>
        <script src="<?= base_url('admin/assets/js/jquery.app.js')?>"></script>

    </body>
</html>