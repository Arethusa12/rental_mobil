<!--== Header Area Start ==-->
<?php $this->load->view('pelanggan/header') ?>
<?php foreach ($mobil as $mobil) {} ?>
<!--== Header Area End   ==-->

<body class="loader-active">


    <!--== Page Title Area Start ==-->
    <section id="page-title-area" class="section-padding overlay">
        <div class="container">
            <div class="row">
                <!-- Page Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>Pemesanan</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                        <p></p>
                    </div>
                </div>
                <!-- Page Title End -->
            </div>
        </div>
    </section>
    <!--== Page Title Area End ==-->

    <!--== Car List Area Start ==-->
       <section id="car-list-area" class="section-padding">
        <div class="container">
            <div class="row">
                
                <!-- Car List Content Start -->
                <div class="col-lg-8">
                    <div class="car-details-content">
                        <h2><?php echo $mobil->nama_m ?> <span class="price">Price <b> Rp <?php echo $mobil->harga?> / Hari </b></span></h2>
                        <div class="car-preview-crousel">
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                            <div class="single-car-preview">
                                <img src="<?= base_url('gambar/'. $mobil->gambar) ?>" alt="JSOFT">
                            </div>
                        </div>
                        <div class="car-details-info">
                           
                        </div>
                    </div>
                </div>
                <!-- Car List Content End -->

                <!-- Sidebar Area Start -->
                <div class="col-lg-4">
                    <div class="sidebar-content-wrap m-t-50">
                        <!-- Single Sidebar Start -->
                        <div class="single-sidebar">
                            <h3>For More Informations</h3>

                            <div class="sidebar-body">
                                <p><i class="fa fa-mobile"></i> (+62) 812873974629 </p>
                                <p><i class="fa fa-clock-o"></i> Mon-Sun 09.00 - 17.00</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>

        <div class="row">                
                <!-- Car List Content Start -->
                <div class="col-lg-8">
                    <div class="car-details-content">
                       
                        
                        <div class="car-details-info">
                           
                            <div class="technical-info">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="tech-info-table">
                                            <table class="table table-bordered">
                                               
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    <form method="post" action="<?php echo base_url('Pelanggan/tambah_pesanan')?>">
                            <div class="review-area">
                                <h3>Pemesanan</h3>
                                <div class="review-star">
                                            
                                </div>
                                <div class="review-form">                                    
                                   
                                        <div class="row">
                                            <input type="hidden" name="id_mobil" value="<?= $mobil->id_mobil?>" >
                                            <input type="hidden" name="id" value="<?= $this->session->userdata('id');?>">
                                            <input type="hidden" name="id_p" value="<?= $mobil->id_p ?>">
                                            <input type="hidden" name="pesan" value="terpesan">

                                            <div class="col-lg-6 col-md-6">
                                                <div class="name-input">
                                                    <input type="text" name="nama_pesan" value="" placeholder="Full Name">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="email-input">
                                                    <input type="email" name="email" value="" placeholder="Email Address">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="name-input">
                                                    <input type="textarea" name="alamat" placeholder="Full Addres">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="name-input">
                                                    <input type="number" name="hari" id="hari" placeholder="Order Day">
                                                </div>
                                            </div>        
                                        </div>  

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="name-input">

                                                    <input type="hidden" name="harga" value="<?= $mobil->harga ?>" placeholder="<?= $mobil->harga ?>" >
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="name-input"> Transaction Picture
                                                    <input type="file" name="bukti">
                                                </div>
                                            </div>
                                        </div>                                    

                                        <div class="input-submit">
                                            <button type="submit">Order Now</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Car List Content End -->
                <!-- Sidebar Area Start -->
                <!-- Sidebar Area End -->
            </div>
        </div>
    </section>

    <!--== Car List Area End ==-->

    <!--== Footer Area Start ==-->
    
            <?php $this->load->view('pelanggan/footer') ?>

    <!--== Footer Area End ==-->


</body>

</html>