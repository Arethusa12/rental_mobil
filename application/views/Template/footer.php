<!-- jQuery  -->
        <script src="<?php echo base_url('Admin/assets/js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/detect.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/fastclick.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/jquery.blockUI.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/waves.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/jquery.slimscroll.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/jquery.scrollTo.min.js')?>"></script>
        <script src="<?php echo base_url('Admin/plugins/switchery/switchery.min.js')?>"></script>

        <!-- Examples -->
        <script src="<?php echo base_url('Admin/plugins/magnific-popup/js/jquery.magnific-popup.min.js')?>"></script>
        <script src="<?php echo base_url('Admin/plugins/jquery-datatables-editable/jquery.dataTables.js')?>"></script>
        <script src="<?php echo base_url('Admin/plugins/datatables/dataTables.bootstrap.js')?>"></script>
        <script src="<?php echo base_url('Admin/plugins/tiny-editable/mindmup-editabletable.js')?>"></script>
        <script src="<?php echo base_url('Admin/plugins/tiny-editable/numeric-input-example.js')?>"></script>

         <!-- Sweet-Alert  -->
        <script src="<?php echo base_url('Admin/plugins/bootstrap-sweetalert/sweet-alert.min.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/pages/jquery.sweet-alert.init.js')?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('Admin/assets/js/jquery.core.js')?>"></script>
        <script src="<?php echo base_url('Admin/assets/js/jquery.app.js')?>"></script>

        <script src="<?php echo base_url('Admin/assets/pages/jquery.datatables.editable.init.js')?>"></script>

        <script type="text/javascript">
            $('#notifikasi').slideDown('slow').delay(5000).slideUp('slow');
        </script>



        </body>
</html>
