<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== Favicon ===-->
    

    <title>ElCarro</title>

    <!--=== Bootstrap CSS ===-->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--=== Vegas Min CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/vegas.min.css')?>" rel="stylesheet">
    <!--=== Slicknav CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/slicknav.min.css')?>" rel="stylesheet">
    <!--=== Magnific Popup CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/magnific-popup.css')?>" rel="stylesheet">
    <!--=== Owl Carousel CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/owl.carousel.min.css')?>" rel="stylesheet">
    <!--=== Gijgo CSS ===-->
    <link href="<?php echo base_url('assets/css/plugins/gijgo.css')?>" rel="stylesheet">
    <!--=== FontAwesome CSS ===-->
    <link href="<?php echo base_url('assets/css/font-awesome.css')?>" rel="stylesheet">
    <!--=== Theme Reset CSS ===-->
    <link href="<?php echo base_url('assets/css/reset.css')?>" rel="stylesheet">
    <!--=== Main Style CSS ===-->
    <link href="<?php echo base_url('assets/style.css')?>" rel="stylesheet">
    <!--=== Responsive CSS ===-->
    <link href="<?php echo base_url('assets/css/responsive.css')?>" rel="stylesheet">


    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="loader-active">

    <!--== Preloader Area Start ==-->
    <div class="preloader">
        <div class="preloader-spinner">
            <div class="loader-content">
                <img src="<?php echo base_url('assets/img/preloader.gif')?>" >
            </div>
        </div>
    </div>
    <!--== Preloader Area End ==-->

    <!--== Header Area Start ==-->
            <?php $this->load->view('header.php') ?>
    <!--== Header Area End ==-->

    <!--== SlideshowBg Area Start ==-->
    <section id="slideslow-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="slideshowcontent">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <h1>BOOK A CAR TODAY!</h1>
                                <p><br></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--== SlideshowBg Area End ==-->

    <!--== About Us Area Start ==-->
    <section id="about-area" class="section-padding">
        <div class="container">
            <div class="row">
                <!-- Section Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>About us</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                        <p> 100 % Perusahaan Anak Bangsa</p>
                    </div>
                </div>
                <!-- Section Title End -->
            </div>

            <div class="row">
                <!-- About Content Start -->
                <div class="col-lg-6">
                    <div class="display-table">
                        <div class="display-table-cell">
                            <div class="about-content">
                                <p> El Carro adalah perusahan anak bangsa yang berdiri di kota sukabumi sejak tahun 2019. Perusahaan ini berkembang dan bertujuan untuk mempermudah pelanggan untuk memesan kendaraan bermotor di berbagai kota dan daerah. Perusahaan ini akan terus melakukan perkembangan dan memberikan yang terbaik bagi para pelanggannya.</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About Content End -->

                <!-- About Video Start -->
                <div class="col-lg-6">
                    <div class="about-image">
                        <img src="<?php echo base_url('assets/img/home-2-about.png')?>" alt="JSOFT">
                    </div>
                </div>
                <!-- About Video End -->
            </div>

            <!-- About Fretutes Start -->
            <div class="about-feature-area">
                <div class="row">
                    <!-- Single Fretutes Start -->
                    
                    <!-- Single Fretutes End -->
                </div>
            </div>
            <!-- About Fretutes End -->
        </div>
    </section>
    <!--== About Us Area End ==-->

    <!--== Partner Area Start ==-->
    <!--== Partner Area End ==-->

    <!--== Services Area Start ==-->
    <!--== Services Area End ==-->

    <!--== Fun Fact Area Start ==-->
    <!--== Fun Fact Area End ==-->

    <!--== Choose Car Area Start ==-->
    <section id="pesan-mobil" class="section-padding">
        <div class="container">
            <div class="row">
                <!-- Section Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>Choose your Car</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                    </div>
                </div>
                <!-- Section Title End -->
            </div>

            <div class="row">
                <!-- Choose Area Content Start -->
                <div class="col-lg-12">
                    <div class="choose-ur-cars">
                        <div class="row">
                            <div class="col-lg-3">
                                <!-- Choose Filtering Menu Start -->
                                <div class="home2-car-filter">
                                    <a href="" data-filter="*" class="active">all</a>
                                    
                                </div>
                                <!-- Choose Filtering Menu End -->
                            </div>

                            <div class="col-lg-9">
                                <!-- Choose Cars Content-wrap -->
                                <div class="row popular-car-gird">
                                     <?php foreach ($mobil as $row) {
                                        if ($row->tag != 'Open') {
                                            $row->tag = 'Booked';
                                        }else{$row->tag = 'Car Detail';}
                                      ?>   
                                    <!-- Single Popular Car Start -->
                                    <div class="col-lg-6 col-md-6 con suv mpv">
                                        <div class="single-popular-car">
                                            <div class="p-car-thumbnails">
                                                <a class="car-hover" href="<?= base_url('gambar/'. $row->gambar) ?>">
                                                    <img src="<?= base_url('gambar/'. $row->gambar) ?>"  alt="JSOFT">
                                                </a>
                                            </div>

                                            <div class="p-car-content">
                                                <h3>
                                                    <a href="#"><?php echo $row->nama_m ?></a>
                                                    <span class="price"><i class="fa fa-tag"></i><?php echo $row->harga ?>/day</span>
                                                </h3>

                                                <h5><?php echo $row->tipe?></h5>

                                                <div class="p-car-feature">
                                                    <a><?php echo $row->tahun ?></a>
                                                    <a><?php echo $row->transmisi ?></a>
                                                    <a><?php echo $row->plat ?></a>
                                                </div>
                                                <a href="<?php echo base_url('Pelanggan/detail/'.$row->id_mobil)?>" class="rent-btn"><?=$row->tag ?></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Single Popular Car End -->
                                <?php } ?>
                                </div>
                                <!-- Choose Cars Content-wrap -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Choose Area Content End -->
            </div>
        </div>
    </section>
    <!--== Choose Car Area End ==-->

    <!--== Pricing Area Start ==-->

    <!--== Pricing Area End ==-->

    <!--== Team Area Start ==-->

    <!--== Team Area End ==-->

    <!--== Mobile App Area Start ==-->
    <!--== Mobile App Area End ==-->

    <!--== Footer Area Start ==-->
    <section id="footer-area">
        <!-- Footer Widget Start -->
        <div class="footer-widget-area">
            <div class="container">
                <div class="row">
                    <!-- Single Footer Widget Start -->
                    
                    <!-- Single Footer Widget End -->

                    <!-- Single Footer Widget Start -->
                    
                    <!-- Single Footer Widget End -->

                    <!-- Single Footer Widget Start -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-footer-widget">
                            <h2>GET TOUCH</h2>
                            <div class="widget-body">
                               

                                <ul class="get-touch">
                                    <li><i class="fa fa-map-marker"></i> Cibereum Hulur Sukabumi, Indonesia</li>
                                    <li><i class="fa fa-mobile"></i> (+62) 812873974629</li>
                                    <li><i class="fa fa-envelope"></i> askthusa@gmail.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Single Footer Widget End -->
                </div>
            </div>
        </div>
        <!-- Footer Widget End -->

        <!-- Footer Bottom Start -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Bottom End -->
    </section>
    <!--== Footer Area End ==-->

    <!--== Scroll Top Area Start ==-->
    <div class="scroll-top">
        <img src="<?php echo base_url('assets/img/scroll-top.png')?>" alt="JSOFT">
    </div>
    <!--== Scroll Top Area End ==-->

    <!--=======================Javascript============================-->
    <!--=== Jquery Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
    <!--=== Jquery Migrate Min Js ===-->
    <script src="<?php echo base_url('assets/js/jquery-migrate.min.js')?>"></script>
    <!--=== Popper Min Js ===-->
    <script src="<?php echo base_url('assets/js/popper.min.js')?>"></script>
    <!--=== Bootstrap Min Js ===-->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <!--=== Gijgo Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/gijgo.js')?>"></script>
    <!--=== Vegas Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/vegas.min.js')?>"></script>
    <!--=== Isotope Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/isotope.min.js')?>"></script>
    <!--=== Owl Caousel Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/owl.carousel.min.js')?>"></script>
    <!--=== Waypoint Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/waypoints.min.js')?>"></script>
    <!--=== CounTotop Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/counterup.min.js')?>"></script>
    <!--=== YtPlayer Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/mb.YTPlayer.js')?>"></script>
    <!--=== Magnific Popup Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/magnific-popup.min.js')?>"></script>
    <!--=== Slicknav Min Js ===-->
    <script src="<?php echo base_url('assets/js/plugins/slicknav.min.js')?>"></script>

    <!--=== Mian Js ===-->
    <script src="<?php echo base_url('assets/js/main.js')?>"></script>

</body>

</html>