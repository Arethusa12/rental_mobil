<!--== Header Area Start ==-->
    <header id="header-area" class="fixed-top">
        <!--== Header Top Start ==-->
        <div id="header-top" class="d-none d-xl-block">
            <div class="container">
                <div class="row">
                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-left">
                        <i class="fa fa-map-marker"></i> Indonesia, IDN
                    </div>
                    <!--== Single HeaderTop End ==-->

                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-mobile"></i> (+62) )82917739362
                    </div>
                    <!--== Single HeaderTop End ==-->

                    <!--== Single HeaderTop Start ==-->
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-clock-o"></i> Mon-Fri 09.00 - 17.00
                    </div>

                    <div>
                        <div class="">
                        <?php 
                            if ($this->session->userdata('status') == 'login' ) { ?>
                                <a href="<?php echo base_url('Welcome/logout')?>"><i> Logout / </i></a>
                        <?php    
                            } else { ?>
                                <a href="<?php echo base_url('Welcome/login')?>"><i> Login / </i></a>
                        <?php
                            }
                         ?>
                            
                        </div>
                    </div>

                    <div>
                        <div class="">
                            <a href="<?php echo base_url('Welcome/login_p')?>"><i> Login Owner  </i></a>
                            
                        </div>
                    </div>

                    <!--== Single HeaderTop End ==-->

                </div>
            </div>
        </div>
        <!--== Header Top End ==-->

        <!--== Header Bottom Start ==-->
        <div id="header-bottom">
            <div class="container">
                <div class="row">
                    <!--== Logo Start ==-->
                    <div class="col-lg-4">
                        <a href="index.php" class="logo">
                            <img src="" >
                        </a>
                    </div>
                    <!--== Logo End ==-->

                    <!--== Main Menu Start ==-->
                    <div class="col-lg-8 d-none d-xl-block">
                        <nav class="mainmenu alignright">
                            <ul>
                                <li><a href="Welcome">Home</a></li>
                                <li><a href="<?php echo base_url('pelanggan/transaksi')?>">Cart</a></li>
                                <li><a href="#pesan-mobil"> Book A Car </a></li>
                            </ul>
                        </nav>
                    </div>
                    <!--== Main Menu End ==-->
                </div>
            </div>
        </div>
        <!--== Header Bottom End ==-->
    </header>
    <!--== Header Area End ==-->