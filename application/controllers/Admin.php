<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

function __construct(){

		parent::__construct();		
		$this->load->model('m_mobil');
		$this->load->model('m_login_p');
		$this->load->helper('url');
		$this->load->library('upload');
 
}

function mobil(){
		$pemilik             = $this->session->userdata('id_p');
		$data['input_mobil'] = $this->m_mobil->join_p($pemilik);

		$this->load->view('pemilik/input_mobil',$data);
}


function dasbor (){
	$this->load->view('pemilik/dashboard');
}
/*
 function profile($id_p)
 {

 	$data['pemilik'] = $this->m_mobil->tampil_user($id_p);
 	$this->load->view ('pemilik/profile', $data);
 }
 */

 function profile(){
 	$data['accounts'] = $this->m_login_p->getAccount()->result();

 	$this->load->view('pemilik/profile',$data);
 	$this->load->view('pemilik/header',$data);
 }


function tambah_mobil(){

	
		$plat      = $this->input->post('plat');
		$tipe      = $this->input->post('tipe');
		$tahun     = $this->input->post('tahun');
		$merk      = $this->input->post('merk');
		$nama_m    = $this->input->post('nama_m');
		$bensin    = $this->input->post('bensin');
		$kursi     = $this->input->post('kursi');
		$transmisi = $this->input->post('transmisi');
		$nama_p    = $this->input->post('nama_p');
		$harga     = $this->input->post('harga');
		$gambar	   = $this->input->post('gambar');
		$tag  	   = $this->input->post('tag');


		$config['upload_path']		=	"./gambar/";
		$config['allowed_types']	=	"gif|png|jpg|jpeg|bmp";
		$config['overwrite']		= 	true;
		$config['max_size']			=	2000000;
		$config['max_height']		=	2000000;
		$config['max_width']		=	2000000;
		$config['file_name']		=	date('Y-m-d')."-".$nama_m;

		$this->upload->initialize($config);
    	$this->load->library('upload', $config);

    	if (!$this->upload->do_upload('gambar')) {
        	$this->session->set_flashdata('error', $this->upload->display_errors());
        	redirect('Admin/mobil');
    	}
		$data = array(
		'id_p'		=> $this->session->userdata('id_p'),
		'plat'      => $plat,
		'tipe'      => $tipe,
		'tahun'     => $tahun,
		'merk'      => $merk,
		'nama_m'    => $nama_m,
		'bensin'    => $bensin,
		'kursi'     => $kursi,
		'transmisi' => $transmisi,
		'nama_p'    => $nama_p,
		'harga'     => $harga,
		'tag'		=> $tag,
		'gambar'	=> $this->upload->data("file_name")
			);
		$this->m_mobil->input_data($data,'input_mobil');
		$this->session->set_flashdata(
			'message',
			"<script>
			window.onload=function(){
				swal('Tambah','Data Berhasil Di Tambah','success')
			}
			</script>"
		);
		redirect('Admin/mobil');
	}


function edit($id_mobil){
		$where = array('id_mobil'=>$id_mobil);

		$data['id_mobil'] = $this->m_mobil->ambil($where,'input_mobil')->result();
		$this->load->view('pemilik/input_mobil',$data);
	}	

private function _sendEmail(){
		$email = $this->input->post('email');
		$config = array(
			'protocol'	=>	'smtp',
			'smtp_host'	=>	'ssl://smtp.googlemail.com',
			'smtp_user'	=>	'rizzkyardyansyah@gmail.com',
			'smtp_pass'	=>	'rizzky12345',
			'smtp_port'	=>	465,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
            'newline' 	=> "\r\n"
		);

		$this->email->initialize($config);

		$this->email->from('rizzkyardyansyah@gmail.com', 'Rizky Ardiansyah');
		$this->email->to($email);

		$this->email->subject('Item Done');
		$this->email->message('Your item hasbeen done to proses');

		if($this->email->send()){
			return true;
		}else{
			echo $this->email->print_debugger();
			die;
		}
	}		

function proses_edit(){
		$id_mobil  = $this->input->post('id_mobil');
		$plat      = $this->input->post('plat');
		$tipe      = $this->input->post('tipe');
		$tahun     = $this->input->post('tahun');
		$merk      = $this->input->post('merk');
		$nama_m    = $this->input->post('nama_m');
		$bensin    = $this->input->post('bensin');
		$kursi     = $this->input->post('kursi');
		$transmisi = $this->input->post('transmisi');
		$nama_p    = $this->input->post('nama_p');
		$harga     = $this->input->post('harga');
		$tanggal   = $this->input->post('tanggal');
		

	
		$data = array(
		'id_mobil'	=> $id_mobil,
		'plat'      => $plat,
		'tipe'      => $tipe,
		'tahun'     => $tahun,
		'merk'      => $merk,
		'nama_m'    => $nama_m,
		'bensin'    => $bensin,
		'kursi'     => $kursi,
		'transmisi' => $transmisi,
		'nama_p'    => $nama_p,
		'harga'     => $harga,
		'tanggal'   => $tanggal
			);
		$where=array('id_mobil'=>$id_mobil);

		$this->m_mobil->update($where,$data,'input_mobil');
		redirect('Admin/mobil');
	}	

function proses_edit_t(){
		$id_detail = $this->input->post('id_detail');
		$nama      = $this->input->post('nama');
		$alamat    = $this->input->post('alamat'); 
		$email     = $this->input->post('email');
		$hari      = $this->input->post('hari');

		$data = array(
		'id_detail' => $id_detail,	
		'nama'		=> $nama,
		'alamat'	=> $alamat,
		'email'		=> $email,	
		'hari'		=> $hari
			);
		$where=array('id_detail'=>$id_detail);

		$this->m_mobil->update($where,$data,'detail_pesan');
		redirect('Admin/d_pesan');
	}	


	function d_pesan(){
		$plg = $this->session->userdata('id_p');
		$data['trans'] = $this->m_mobil->join_d($plg);
		$this->load->view('pemilik/data_pesan', $data);

	}

	function hapus($id_mobil){
		$row = $this->m_mobil->get_mobil($id_mobil);
		if ($row) {
			unlink('./gambar/'.$row->gambar);
			$where=array('id_mobil'=>$id_mobil);
			$this->m_mobil->hapus($where,'input_mobil');
			$this->session->set_flashdata(
			'message',
			"<script>
			window.onload=function(){
				swal('Hapus','Data Berhasil Di Hapus','success')
			}
			</script>"
		);
			redirect('Admin/mobil');
		}else{
			echo "<script>alert('Gambar Tidak Ditemukan');</script>";
		}
	}

}