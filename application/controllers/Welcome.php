<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
	parent::__construct();
	$this->load->model('m_mobil');
	$this->load->helper('url');

}

	public function index()
	{
		$data['mobil'] = $this->m_mobil->tampil_data()->result();
		$this->load->view('index',$data);
	}

	public function login()
	{
		$this->load->view('login');
	}


	public function login_p()
	{
		$this->load->view('login_pemilik');
	}


	public function register()
	{
		$this->load->view('register');
	}

	public function register_p()
	{
		$this->load->view('register_pemilik');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('Welcome/index'));
	}
}
