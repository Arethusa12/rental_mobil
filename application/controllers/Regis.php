<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regis extends CI_Controller {

function __construct(){

		parent::__construct();		
		$this->load->model('m_regis');
		$this->load->helper('url');
 
}

public function register (){
	$this->load->view('register');
}


function tambah_register(){
		$nama      		= $this->input->post('nama');
		$email      	= $this->input->post('email');
		$username    	= $this->input->post('username');
		$password     	= $this->input->post('password');
		$nik	    	= $this->input->post('nik');
		$ktp			= $this->input->post('ktp');

		$config['upload_path']		=	"./gambar/";
		$config['allowed_types']	=	"gif|png|jpg|jpeg|bmp";
		$config['overwrite']		= 	true;
		$config['max_size']			=	2000000;
		$config['max_height']		=	2000000;
		$config['max_width']		=	2000000;
		$config['file_name']		=	date('Y-m-d')."-".$nik;

		$this->upload->initialize($config);
    	$this->load->library('upload', $config);

    	if (!$this->upload->do_upload('ktp')) {
        	$this->session->set_flashdata('error', $this->upload->display_errors());
        	
    	}

		$data = array(
			'nama' => $nama,
			'email' => $email,
			'username' => $username,
			'password' => md5($password),
			'nik' => $nik,
			'ktp' => $this->upload->data("file_name")
			
			);
		$this->m_regis->input_data($data,'users');
		$this->session->set_flashdata(
			'message',
			"<script>
			window.onload=function(){
				swal('Tambah','Register Success Please Login','success')
			}
			</script>"
		);
		redirect('Login/masuk');
	}
}	