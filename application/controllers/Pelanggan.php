<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

function __construct() {
	parent::__construct();
	$this->load->model('m_mobil');
	$this->load->helper('url');
	$this->load->library('upload');

	}

function catchMobil($id_mobil){

	$data_session = array(

		'id_mobil' => $id_mobil

	);

	$this->session->set_userdata($data_session);
	redirect("Pelanggan/pilih");

}

function pilih()
 {
 	$id_mobil = $this->session->userdata("id_mobil");
 // 	if($this->session->userdata('status') != "login"){
	// 		redirect(base_url("login/masuk"));
	// 	}
	$data['id']       = $this->m_mobil->get_user($id_mobil);
 	// $data['id_mobil'] = $this->m_mobil->get_mobil($id_mobil);
 	// $data['mobil'] = $this->m_mobil->get_mobil($id_mobil);
 	$data ['mobil'] = $this->m_mobil->getCatchMobil()->result(); 
 	$this->load->view('pelanggan/pilih_mobil',$data);
 }

  function pesan($id_pesan)
 {
 	if($this->session->userdata('status') != "login"){
			redirect(base_url("login/masuk"));
		}

 	$data['pesan'] = $this->m_mobil->get_pesan($id_pesan);
 	$this->load->view('pelanggan/pilih_mobil',$data);
 }

 function tambah_pesanan(){

 		
		$id_mobil   = $this->input->post('id_mobil');
		$id_p 		= $this->input->post('id_p');
		$nama_pesan = $this->input->post('nama_pesan');
		$email      = $this->input->post('email');
		$alamat     = $this->input->post('alamat');
		$hari       = $this->input->post('hari');
		$harga      = $this->input->post('harga');
		$pesan  	= $this->input->post('pesan');
		$bukti		= $this->input->post('bukti');

		$total = $hari * $harga;
		
		$data = array(
		'id'	    => $this->session->userdata('id'),
		'id_p'      => $id_p,
		'id_mobil'  => $id_mobil,
		'nama_pesan'=> $nama_pesan,
		'email'     => $email,
		'alamat'    => $alamat,
		'harga'		=> $harga,
		'hari'      => $hari,
		'total_harga' => $total,
		'bukti'		=> $this->upload->data("file_name")
		);	

		$this->catchUpdate();
		$this->m_mobil->input_pesan($data,'detail_pesan');
		redirect('Pelanggan/transaksi');
	}

	private function upload_bukti(){
		$config['upload_path'] = './gambar/';
		$config['allowed_types'] = 'gif|png|jpg|jpeg|bmp';
		// $config['max_size']  = '100';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';
		$config['file_name']		= round(microtime(true)*1000);
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
			
			return $this->upload->data('file_name');
		}
	}
	

	function beres(){

		$this->catchUpdate();
		redirect('pelanggan/transaksi');
	}


	function transaksi (){
		$pelanggan          = $this->session->userdata('id');
		$data['trans']      = $this->m_mobil->join($pelanggan);
		$this->load->view('pelanggan/d_transaksi', $data);
	}

	function catchUpdate(){
		$mobil = $this->m_mobil->getCatchMobil()->result(); 
		foreach ($mobil as $mbl) {}

			if ($mbl->tag == 'Open') {
			$where = array('id_mobil' => $mbl->id_mobil);
			$data = array('tag' => 'Closed');
			return $this->m_mobil->update($where,$data,'input_mobil');		
			}else{
			$where = array('id_mobil' => $mbl->id_mobil);
			$data = array('tag' => 'Open');
			return $this->m_mobil->update($where,$data,'input_mobil');

			}
		

		

	}

  function detail($id)
 {
 	$data['mobil'] = $this->m_mobil->get_mobil($id);
 	$this->load->view ('pelanggan/car_detail', $data);
 }

 function struk($id){

 	$data['nota'] = $this->m_mobil->get_nota($id);
 	$this->load->view('pelanggan/struk', $data);
 }

}

