<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regis_p extends CI_Controller {

function __construct(){

		parent::__construct();		
		$this->load->model('m_regis_p');
		$this->load->helper('url');
		$this->load->library('upload');
 
}

public function register (){
	$this->load->view('register_pemilik');
}


function tambah_register(){
		$nama      		= $this->input->post('nama');
		$email      	= $this->input->post('email');
		$username    	= $this->input->post('username');
		$password     	= $this->input->post('password');
		$nik	    	= $this->input->post('nik');
		$gambar			= $this->input->post('gambar');

		$config['upload_path']		=	"./gambar/";
		$config['allowed_types']	=	"gif|png|jpg|jpeg|bmp";
		$config['overwrite']		= 	true;
		$config['max_size']			=	2000000;
		$config['max_height']		=	2000000;
		$config['max_width']		=	2000000;
		$config['file_name']		=	date('Y-m-d');

		$this->upload->initialize($config);
    	$this->load->library('upload', $config);

    	if (!$this->upload->do_upload('gambar')) {
        	$this->session->set_flashdata('error', $this->upload->display_errors());
        	redirect('Regis_p/register');
    	}

		$data = array(
			'nama' => $nama,
			'email' => $email,
			'username' => $username,
			'password' => md5($password),
			'nik' => $nik,
			'gambar'	=> $this->upload->data("file_name")
			
			);
		$this->m_regis_p->input_data($data,'users_pemilik');
		redirect('Login_Pemilik/masuk');
	}
}	